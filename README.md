# Generate the Course Information from the Timetable

[![pipeline status](https://gitlab.com/jordonedavidson/timetable_to_csv/badges/master/pipeline.svg)](https://gitlab.com/jordonedavidson/timetable_to_csv/-/commits/master)

## Requirements

- PHP 7.3
- [Composer](https://getcomposer.org)

Run `composer install`

Start a local server with `symfony server:start --port=8050`

Visit [localhost:8050/timetable](http://localhost:8050/timetable) to generate the `WinterTimetable.csv` file.
