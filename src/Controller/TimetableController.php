<?php

namespace App\Controller;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class TimetableController extends AbstractController
{
    /**
     * Read in the timedable from the api and return a csv of the desired data.
     *
     * @Route("/timetable", name="timetable")
     *
     * @return Response
     */
    public function index(): Response
    {
        // Filename for the returned file.
        $filename = 'WinterTimetable.csv';

        // First line of the output data.
        $timetable = "Subject,Course,Section,Instructor\n";

        // Http client to retrieve the API data
        $client = new Client();

        // Get the data.
        // It is returned as a JSON string inside an XML document
        $result = $client->get('https://www.mta.ca/WebServices/TimeTableService.asmx/getTimeTableWinter');

        // Extract the JSON object from the result
        $xml = simplexml_load_string((string) $result->getBody());
        $data = json_decode($xml->__toString());

        // Roll through the courses array and write the appropriate data to the output string.
        foreach ($data as $course) {
            $code = (string) $course->course;

            // We don't want thesis (4990) or lab (xxxL) courses so excude them.
            if ("4990" != substr($code, 0, 4) && "L" != substr($code, -1)) {
                // Add the course to the file.
                $timetable .= $course->subject
                    . ','
                    . $course->course
                    . ','
                    . $course->section
                    . ','
                    . $course->email
                    . "\n";
            }
        }

        // Prepare the file response.
        $response = new Response($timetable);

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        $response->headers->set('Content-Disposition', $disposition);

        // Serve the file.
        return $response;
    }
}
